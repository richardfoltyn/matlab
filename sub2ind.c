/* LEAVE THIS LINE ALWAYS UNCHANGED:  */
#include "cfunc.h"

#define NGRID prhs[0]
#define LINIDX plhs[0]
#define NSUB (nrhs-1)
#define IDX1 prhs[1]


/* LEAVE THIS LINE ALWAYS UNCHANGED:  */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

  int i, j;
  double *idx;
  size_t ndim;
  int nGrid_nr, nGrid_nc;
  double *nGrid, *ii, *curr;
  unsigned long int nr, nc, nelem;
  unsigned int tmp;
  
  if (nrhs < 2) {
    mexErrMsgTxt("At least two input arguments required!");
  }
  
  nGrid = mxGetPr(NGRID);
  nGrid_nr = mxGetM(NGRID);
  nGrid_nc = mxGetN(NGRID);
  ndim = mxGetNumberOfElements(NGRID);
  
  if (ndim < NSUB) {
    mexErrMsgTxt("Array size smaller than number of index vectors!");
  }
  
  nr = mxGetM(IDX1);
  nc = mxGetN(IDX1);
  nelem = nc * nr;
  
  if( nr > 1 && nc > 1) {
    mexErrMsgTxt("Index vectors must be of size n-by-1 or 1-by-n!");
  }

  
  for(i = 2; i < nrhs; i++) {
    unsigned long int lnc, lnr;
    lnr = mxGetM(prhs[i]);
    lnc = mxGetN(prhs[i]);
    
    if( nr != lnr || nc != lnc) {
      mexErrMsgTxt("Index vectors must be of same size!");
    }
  }
  
  LINIDX = mxCreateDoubleMatrix(nr,nc, mxREAL); 
  idx = mxGetPr(LINIDX);

  for(i = 0; i < nelem; i++ ) {
    
    idx[i] = 0;
    
    for(j = NSUB; j > 1; j--) {
      tmp = (unsigned int) nGrid[j-2];
      curr = mxGetPr(prhs[j]);
      idx[i] += (curr[i] - 1);
      idx[i] *= tmp;
    }
    curr = mxGetPr(prhs[1]);
    idx[i] += curr[i];
  }
}


