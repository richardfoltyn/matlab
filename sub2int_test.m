% Unit test for sub2ind
% To run it, make sure the native implementation is called sub2ind2 and is
% in the search path.

dims = [10 10 10];

idx = randi(prod(dims), 100, 1);
[row, col] = ind2sub(dims, idx);

idx2 = sub2ind2(dims, row, col);
assert(all(idx == idx2));

nsim = 1000;
tic
for i=1:nsim
   j = sub2ind2(dims, row, col);
end
ntime = toc();

tic
for i=1:nsim
   j = sub2ind(dims, row, col);
end
mtime = toc();

fprintf(1,'Native time:  %5.4e sec\n', ntime/1000);
fprintf(1,'Matlab time: %5.4e sec\n', mtime/1000);
fprintf(1,'Speed-up: %5.3f\n', mtime/ntime);

