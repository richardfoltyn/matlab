/* Written by Michael Reiter, IHS Vienna */

/*
  To make a matlab function from this file, type
  "mex weightmc.c"
  on the command level or from matlab on tornasol.
  That's all. (It produces a file value_c.mexsol)
*/

/* LEAVE THIS LINE ALWAYS UNCHANGED:  */
#include "cfunc.h"


/* LEAVE THIS LINE ALWAYS UNCHANGED:  */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

  int i, j, k;
  DECL_MAT(x);
  DECL_MAT(y);
  /* DECLARE OUTPUT MATRICES: */
  DECL_OUTPUT(xx);
  DECL_OUTPUT(yy);

  /* FUNCTION GETS 2 INPUTS AND 1 OUTPUT ARGUMENT:  */
  CHECK_ARGN(2,2);


  GET_ARG_MAT(x,1);
  GET_ARG_MAT(y,2);

  
  CREATE_OUTPUT(1,xx,x_nr*y_nr,x_nc,REAL);
  CREATE_OUTPUT(2,yy,x_nr*y_nr,y_nc,REAL);

  INCR(j,1,x_nc){
    int lauf=1;
    INCR(k,1,y_nr){
      INCR(i,1,x_nr){
	MATEL(xx,lauf++,j) = MATEL(x,i,j);
      }
    }
  }

  INCR(j,1,y_nc){
    int lauf=1;
    INCR(i,1,y_nr){
      INCR(k,1,x_nr){
	MATEL(yy,lauf++,j) = MATEL(y,i,j);
      }
    }
  }


}


