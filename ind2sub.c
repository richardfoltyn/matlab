/* Reimplementation of MATLABs built-in index conversion functions
 * in C as those are somewhat slow.
 * Author: Richard Foltyn
 */

/* LEAVE THIS LINE ALWAYS UNCHANGED:  */
#include "mex.h"

#define NGRID prhs[0]
#define IDX prhs[1]


/* LEAVE THIS LINE ALWAYS UNCHANGED:  */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

  int i, j;
  unsigned long int lval;
  // unsigned int nn[nlhs];
  unsigned int tmp;
  size_t ndim;
  int nGrid_nr, nGrid_nc, ii_nr, ii_nc, ii_max;
  double *nGrid, *ii, *curr;
  unsigned long int maxlval;
  
  if (nrhs != 2) {
    mexErrMsgTxt("Exactly two input arguments required!");
  }
  
  nGrid = mxGetPr(NGRID);
  nGrid_nr = mxGetM(NGRID);
  nGrid_nc = mxGetN(NGRID);
  ndim = mxGetNumberOfElements(NGRID);
  
  ii = mxGetPr(IDX);
  ii_nr = mxGetM(IDX);
  ii_nc = mxGetN(IDX);
  
  
  if (nGrid_nr < nlhs && nGrid_nc < nlhs) {
    mexErrMsgTxt("Array size smaller than number of output arguments!");
  }
  
  if( ii_nr > 1 && ii_nc > 1) {
    mexErrMsgTxt("Linear index must be an n-by-1 or 1-by-n vector!");
  }
  
  maxlval = 1;
  for(i = 0; i < ndim; i++) {
    maxlval *= (unsigned int) nGrid[i];
  }
  
  for(i = 0; i < nlhs; i++) {
    plhs[i] = mxCreateDoubleMatrix(ii_nr,ii_nc, mxREAL); 
  }
  
  ii_max = (ii_nr >= ii_nc ? ii_nr : ii_nc);
  for(i = 0; i < ii_max; i++ ) {
    lval = ((unsigned long) ii[i]) - 1;

    if(lval >= maxlval) {
      mexErrMsgTxt("Linear index exceeds array dimensions!");
    }

    for(j = 0; j < nlhs; j++){
      tmp = (unsigned int) nGrid[j];
      curr = mxGetPr(plhs[j]);
      curr[i] = (j < nlhs - 1) ? (lval % tmp) + 1 : lval + 1;
      lval /= tmp;
    }
  }
}


